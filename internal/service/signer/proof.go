package signer

import (
	"context"
	"fmt"

	"github.com/hyperledger/aries-framework-go/pkg/doc/signature/jsonld"
	ariesigner "github.com/hyperledger/aries-framework-go/pkg/doc/signature/signer"
	"github.com/hyperledger/aries-framework-go/pkg/doc/signature/suite"
	"github.com/hyperledger/aries-framework-go/pkg/doc/signature/suite/jsonwebsignature2020"
	"github.com/hyperledger/aries-framework-go/pkg/doc/verifiable"

	"gitlab.eclipse.org/eclipse/xfsc/tsa/golib/errors"
)

func (s *Service) addCredentialProof(ctx context.Context, issuer, namespace, keyname string, vc *verifiable.Credential) (*verifiable.Credential, error) {
	key, err := s.getKey(ctx, namespace, keyname)
	if err != nil {
		return nil, err
	}

	proofContext, err := s.proofContext(issuer, namespace, key.Name)
	if err != nil {
		return nil, err
	}

	if err := vc.AddLinkedDataProof(proofContext, jsonld.WithDocumentLoader(s.docLoader)); err != nil {
		return nil, err
	}

	return vc, nil
}

func (s *Service) addPresentationProof(ctx context.Context, issuer, keyNamespace, keyName string, vp *verifiable.Presentation) (*verifiable.Presentation, error) {
	key, err := s.getKey(ctx, keyNamespace, keyName)
	if err != nil {
		return nil, err
	}

	proofContext, err := s.proofContext(issuer, keyNamespace, key.Name)
	if err != nil {
		return nil, err
	}

	if err := vp.AddLinkedDataProof(proofContext, jsonld.WithDocumentLoader(s.docLoader)); err != nil {
		return nil, err
	}

	return vp, nil
}

func (s *Service) getKey(ctx context.Context, namespace, keyname string) (*VaultKey, error) {
	key, err := s.vault.Key(ctx, namespace, keyname)
	if err != nil {
		return nil, errors.New("error getting signing key", err)
	}

	if !s.supportedKey(key.Type) {
		return nil, fmt.Errorf("unsupported key type: %s", key.Type)
	}

	return key, nil
}

// proofContext is used to create proofs.
func (s *Service) proofContext(issuer, namespace, key string) (*verifiable.LinkedDataProofContext, error) {
	sigSuite, sigType, err := s.signatureSuite(namespace, key)
	if err != nil {
		return nil, err
	}

	proofContext := &verifiable.LinkedDataProofContext{
		Suite:                   sigSuite,
		SignatureType:           sigType,
		SignatureRepresentation: verifiable.SignatureJWS,
		VerificationMethod:      issuer + "#" + key,
	}

	return proofContext, nil
}

// signatureSuite is used to create digital signatures on proofs.
func (s *Service) signatureSuite(namespace, key string) (sigSuite ariesigner.SignatureSuite, sigType string, err error) {
	signer := s.vault.WithKey(namespace, key)
	sigType = "JsonWebSignature2020"
	sigSuite = jsonwebsignature2020.New(suite.WithSigner(signer))
	return sigSuite, sigType, nil
}
