package config

import "time"

type Config struct {
	HTTP    httpConfig
	Vault   vaultConfig
	Metrics metricsConfig
	Auth    authConfig
	Cred    credentialsConfig
	Train   trainConfig

	LogLevel string `envconfig:"LOG_LEVEL" default:"INFO"`
}

type httpConfig struct {
	Host         string        `envconfig:"HTTP_HOST"`
	Port         string        `envconfig:"HTTP_PORT" default:"8080"`
	IdleTimeout  time.Duration `envconfig:"HTTP_IDLE_TIMEOUT" default:"120s"`
	ReadTimeout  time.Duration `envconfig:"HTTP_READ_TIMEOUT" default:"10s"`
	WriteTimeout time.Duration `envconfig:"HTTP_WRITE_TIMEOUT" default:"10s"`
}

type vaultConfig struct {
	Addr  string `envconfig:"VAULT_ADDR" required:"true"`
	Token string `envconfig:"VAULT_TOKEN" required:"true"`

	// Supported signing key types. As Vault or HSM may contain different
	// key types like symmetric encryption or hmac keys, we want to limit
	// key fetching to asymmetric signing key types only.
	SupportedKeys []string `envconfig:"VAULT_SUPPORTED_KEYS" required:"true"`
}

type metricsConfig struct {
	Addr string `envconfig:"METRICS_ADDR" default:":2112"`
}

type authConfig struct {
	Enabled         bool          `envconfig:"AUTH_ENABLED" default:"false"`
	JwkURL          string        `envconfig:"AUTH_JWK_URL"`
	RefreshInterval time.Duration `envconfig:"AUTH_REFRESH_INTERVAL" default:"1h"`
}

type credentialsConfig struct {
	Verifiers []string `envconfig:"CREDENTIAL_VERIFIERS"`
}

type trainConfig struct {
	Addr         string   `envconfig:"TRAIN_ADDR"`
	TrustSchemes []string `envconfig:"TRAIN_TRUST_SCHEMES"`
}
