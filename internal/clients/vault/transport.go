package vault

import "strconv"

type getKeyResponse struct {
	Data struct {
		Name string                 `json:"name"`
		Type string                 `json:"type"`
		Keys map[string]interface{} `json:"keys"`
	} `json:"data"`
}

// lastPublicKeyVersion iterates the map with key versions and
// returns the latest public key.
func (r *getKeyResponse) lastPublicKeyVersion() string {
	var lastVerString string
	var lastVerInt int
	for ver := range r.Data.Keys {
		verInt, err := strconv.Atoi(ver)
		if err != nil {
			continue
		}

		if verInt > lastVerInt {
			lastVerInt = verInt
			lastVerString = ver
		}
	}

	// check if there is PublicKey field
	key, ok := r.Data.Keys[lastVerString].(map[string]interface{})
	if !ok {
		return ""
	}

	pub, ok := key["public_key"].(string)
	if !ok {
		return ""
	}

	return pub
}

type getKeysResponse struct {
	Data struct {
		Keys []string `json:"keys"`
	} `json:"data"`
}
