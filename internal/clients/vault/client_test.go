// nolint:revive
package vault_test

import (
	"context"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.eclipse.org/eclipse/xfsc/tsa/golib/errors"
	"gitlab.eclipse.org/eclipse/xfsc/tsa/signer/internal/clients/vault"
	"gitlab.eclipse.org/eclipse/xfsc/tsa/signer/internal/service/signer"
)

func TestClient_Namespaces(t *testing.T) {
	tests := []struct {
		name    string
		handler http.HandlerFunc

		result  []string
		errkind errors.Kind
		errtext string
	}{
		{
			name: "error when getting secret engines from vault",
			handler: func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(http.StatusInternalServerError)
				_, _ = w.Write([]byte("unknown error"))
			},
			errkind: errors.Internal,
			errtext: "unknown error",
		},
		{
			name: "vault returns one secret engine not of type transit",
			handler: func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(http.StatusOK)
				_, _ = w.Write([]byte(`{"data":{"secret/":{"type":"kv"}}}`))
			},
		},
		{
			name: "vault returns multiple secret engine and one is of type transit",
			handler: func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(http.StatusOK)
				_, _ = w.Write([]byte(`{"data":{"cubbyhole/":{"type":"kv"},"hello/":{"type":"transit"}}}`))
			},
			result: []string{"hello"},
		},
		{
			name: "vault returns multiple secret engine all with type transit",
			handler: func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(http.StatusOK)
				_, _ = w.Write([]byte(`{"data":{"transit/":{"type":"transit"},"hello/":{"type":"transit"}}}`))
			},
			result: []string{"transit", "hello"},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			vaultsrv := httptest.NewServer(test.handler)
			client, err := vault.New(vaultsrv.URL, "token", false, http.DefaultClient)
			assert.NoError(t, err)

			res, err := client.Namespaces(context.Background())
			if err != nil {
				e, ok := err.(*errors.Error)
				assert.True(t, ok)
				assert.Equal(t, test.errkind, e.Kind)
				assert.Contains(t, e.Message, test.errtext)
			} else {
				assert.Empty(t, test.errtext)

				// ordering of the result list may be different on every execution,
				// because it's populated by iterating over a map, we must check explicitly
				// every value if exists in the result - we can't just use assert.Equal
				assert.Len(t, res, len(test.result))
				for _, expected := range test.result {
					assert.Contains(t, res, expected)
				}
			}
		})
	}
}

func TestClient_NamespaceKeys(t *testing.T) {
	tests := []struct {
		name      string
		namespace string
		key       string
		handler   http.HandlerFunc

		result  []string
		errkind errors.Kind
		errtext string
	}{
		{
			name: "given namespace is empty",
			handler: func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(http.StatusOK)
			},
			errtext: "keys namespace is missing",
		},
		{
			name:      "vault returns 404 Not Found",
			namespace: "transit",
			handler: func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(http.StatusNotFound)
			},
			errkind: errors.NotFound,
			errtext: "404",
		},
		{
			name:      "vault returns 500 Internal Server Error",
			namespace: "transit",
			handler: func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(http.StatusInternalServerError)
			},
			errkind: errors.Internal,
			errtext: "500",
		},
		{
			name:      "vault returns a list of keys",
			namespace: "transit",
			handler: func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(http.StatusOK)
				_, _ = w.Write([]byte(`{"data":{"keys":["key123","mykey"]}}`))
			},
			result: []string{"mykey", "key123"},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			vaultsrv := httptest.NewServer(test.handler)
			client, err := vault.New(vaultsrv.URL, "token", false, http.DefaultClient)
			assert.NoError(t, err)

			res, err := client.NamespaceKeys(context.Background(), test.namespace)
			if err != nil {
				assert.Nil(t, res)

				e, ok := err.(*errors.Error)
				require.True(t, ok)
				assert.Equal(t, test.errkind, e.Kind)
				assert.Contains(t, e.Error(), test.errtext)
			} else {
				assert.Empty(t, test.errtext)

				// ordering of the result list may be different on every execution,
				// because it's populated by iterating over a map, we must check explicitly
				// every value if exists in the result - we can't just use assert.Equal
				assert.Len(t, res, len(test.result))
				for _, expected := range test.result {
					assert.Contains(t, res, expected)
				}
			}
		})
	}
}

func TestClient_Key(t *testing.T) {
	tests := []struct {
		name      string
		namespace string
		key       string
		handler   http.HandlerFunc

		result  *signer.VaultKey
		errkind errors.Kind
		errtext string
	}{
		{
			name:    "key name is empty",
			errkind: errors.Unknown,
			errtext: "key namespace or name is missing",
		},
		{
			name:    "namespace is empty",
			errkind: errors.Unknown,
			errtext: "key namespace or name is missing",
		},
		{
			name:      "key not found",
			namespace: "transit",
			key:       "key1",
			handler: func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(http.StatusNotFound)
				_, _ = w.Write([]byte("key is not found"))
			},
			errkind: errors.NotFound,
			errtext: "key is not found",
		},
		{
			name:      "unauthorized request",
			namespace: "transit",
			key:       "key1",
			handler: func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(http.StatusUnauthorized)
				_, _ = w.Write([]byte("unauthorized request"))
			},
			errkind: errors.Unauthorized,
			errtext: "unauthorized request",
		},
		{
			name:      "internal error",
			namespace: "transit",
			key:       "key1",
			handler: func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(http.StatusInternalServerError)
				_, _ = w.Write([]byte("internal error"))
			},
			errkind: errors.Internal,
			errtext: "internal error",
		},
		{
			name:      "key is retrieved",
			namespace: "transit",
			key:       "key1",
			handler: func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(http.StatusOK)
				_, _ = w.Write([]byte(`{"data":{"name":"key123","type":"ed25519"}}`))
			},
			result: &signer.VaultKey{
				Name: "key123",
				Type: "ed25519",
			},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			vaultsrv := httptest.NewServer(test.handler)
			client, err := vault.New(vaultsrv.URL, "token", false, http.DefaultClient)
			assert.NoError(t, err)

			res, err := client.Key(context.Background(), test.namespace, test.key)
			if err != nil {
				assert.Nil(t, res)

				e, ok := err.(*errors.Error)
				assert.True(t, ok)
				assert.Equal(t, test.errkind, e.Kind)
				assert.Contains(t, e.Error(), test.errtext)
			} else {
				assert.Empty(t, test.errtext)
				assert.Equal(t, test.result, res)
			}
		})
	}
}

func TestClient_WithKey(t *testing.T) {
	vaultsrv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		assert.Equal(t, r.URL.Path, "/v1/mytest-namespace/sign/mytest-key123")
		w.WriteHeader(http.StatusNotFound)
	}))

	c1, err := vault.New(vaultsrv.URL, "token", false, http.DefaultClient)
	assert.NoError(t, err)

	c2 := c1.WithKey("mytest-namespace", "mytest-key123")
	_, _ = c2.Sign([]byte("nop"))
}

func TestClient_Sign(t *testing.T) {
	tests := []struct {
		name    string
		data    []byte
		handler http.HandlerFunc

		result  []byte
		errkind errors.Kind
		errtext string
	}{
		{
			name: "key not found",
			handler: func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(http.StatusNotFound)
				_, _ = w.Write([]byte("key is not found"))
			},
			errkind: errors.NotFound,
			errtext: "key is not found",
		},
		{
			name: "unauthorized request",
			handler: func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(http.StatusUnauthorized)
				_, _ = w.Write([]byte("unauthorized request"))
			},
			errkind: errors.Unauthorized,
			errtext: "unauthorized request",
		},
		{
			name: "internal error",
			handler: func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(http.StatusInternalServerError)
				_, _ = w.Write([]byte("internal error"))
			},
			errkind: errors.Internal,
			errtext: "internal error",
		},
		{
			name: "successful signing",
			handler: func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(http.StatusOK)
				_, _ = w.Write([]byte(`{"data":{"signature":"aGVsbG8="}}`))
			},
			result: []byte("hello"),
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			vaultsrv := httptest.NewServer(test.handler)
			client, err := vault.New(vaultsrv.URL, "token", false, http.DefaultClient)
			assert.NoError(t, err)

			res, err := client.Sign(test.data)
			if err != nil {
				assert.Nil(t, res)

				e, ok := err.(*errors.Error)
				assert.True(t, ok)
				assert.Equal(t, test.errkind, e.Kind)
				assert.Contains(t, e.Error(), test.errtext)
			} else {
				assert.Empty(t, test.errtext)
				assert.Equal(t, test.result, res)
			}
		})
	}
}
